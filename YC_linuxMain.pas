unit YC_linuxMain;

{$MODE Delphi}

{
Copyright (C) 2003-2019 John Drew VK5DJ
This code is part of YagiCalcL.
YagiCalcL is hereby released under the Gnu Public Licence v.3 -
   see GPLv3.txt or http://www.gnu.org/licenses/gpl-3.0.txt
}
{
This program is a cut down version of my Yagi Calculator. It includes all the routines to provide for the calculation
of reflector, dipole(s) and directors for a DL6WU style yagi. It includes formulae from a number of sources including
DL6WU - the original article in VHF Communications (March 1982) entitled Extremely Long Yagi Antennas, a BASIC program
by KY4Z and W6NBI, the ARRL Handbook, K5OE's web site, VK3AUU, DJ9BV, VK2ZRH, VK2AES, and many conversations with informed
amateurs over many years. My task was to bring it all together yet recognise all who helped.

The functions take into account various mounting methods but I have chosen not to adjust the length of the dipole for
different booms as there is always one side of a folded dipole free of the boom and because the radiator is a relatively
non critical element. I have no data to suggest how to handle this. In any case I believe that it is always
good practice to insulate the folded dipole completely so currents find their own electrical centre at the attachment point.

By using just one form in this Delphi program I have avoided (for the moment) complexities for the Linux version.
Jack VE3UKD is providing his Linux expertise for the conversion. In the meantime I'm thinking about creating an Android version.

John
VK5DJ  25 Jan 2019
}
interface

uses
  {Winapi.}LCLIntf, LCLType, LMessages, {Winapi.}Messages, {System.}SysUtils, {System.}Variants, {System.}Classes, {Vcl.}Graphics,
  {Vcl.}Controls, {Vcl.}Forms, {Vcl.}Dialogs, {Vcl.}StdCtrls, {Vcl.}ExtCtrls,
  {Vcl.Imaging.GIFImg,} math;

type
  Spacing=Array[1..14] of extended;
  TForm1 = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    RadioGroup1: TRadioGroup;
    ListBox1: TListBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Button1: TButton;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    RadioGroup2: TRadioGroup;
    RadioGroup3: TRadioGroup;
    RadioGroup4: TRadioGroup;
    RadioGroup5: TRadioGroup;
    Label10: TLabel;
    Label11: TLabel;
    Edit6: TEdit;
    Edit7: TEdit;
    Button2: TButton;
    Panel4: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Edit8: TEdit;
    Button3: TButton;
    Label14: TLabel;
    Label17: TLabel;
    Edit9: TEdit;
    Edit10: TEdit;
    Label18: TLabel;
    Label19: TLabel;
    Edit11: TEdit;
    Edit12: TEdit;
    Label16: TLabel;
    Memo1: TMemo;
    Label15: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure RadioGroup2Click(Sender: TObject);
    procedure RadioGroup3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

const
   VersionNo = '1.00';                  //provide basic calculations for the Yagi Calculator
   Spaces:Spacing=(0.075,0.18,0.215,0.250,0.280,0.3,0.315,0.330,0.345,0.360,0.375,
                   0.385,0.390,0.395); //after No.14 director all spaced at 0.4

var                                    //variables beginning with "b" are integers, or "f" for float
  Form1: TForm1;
  fFreq              :extended;        //design frequency
  fLambda            :extended;        //wavelength
  fCrossSectionEl    :extended;        //width of Dir/Ref
  fCrossSectionDip   :extended;        //diam section type of dipole
  fDiamBend          :extended;        //bend dima of folded dipole
  fDipoleGap         :extended;        //dipole gap
  bNoDirectors       :shortint;        //no of directors
  fBoomSection       :extended;        //distance across boom
  bBoomType          :shortint;        //square or round (0=square, 1=round)
  fBoomWidth         :extended;        //width of boom mm
  bDirShape          :shortint;        //round(0), square(1) or flat material(2) directors
  bDirMount          :shortint;        //bonded(0), insulated(1), non metal mount(2) directors
  fVelFactor         :extended;        //velocity factor for coax calculation
  bDipoleShape       :shortint;        //round(0), square(1) or flat material(2) dipole
  bDipoleMount       :shortint;        //same as Director(0) or fully isulated(1)
  fWidthFlatEl       :extended;        //width of flat element
  fThickFlatEl       :extended;        //width of flat element
  fWidthFlatDip      :extended;        //width of flat element
  fThickFlatDip      :extended;        //width of flat element
  fReflectorLength   :extended;        //length of reflector in mm
  fFoldedDipoleLength:extended;        //folded dipole in mm
  fSingleDipoleLength:extended;        //singledipole in mm
  fDirectorLengths   :Array[1..100] of extended; //array of directors
  fDirectorDistances :Array[1..100] of extended; //array of director
  fAddedLength       :extended;        //correction factor for boom effect
  fCuttingAccuracy   :extended;        //advice on cutting accuracy
  fBoomLength        :extended;        //drilling distance along boom
  fDistanceReflector :extended;        //distance of reflector from dipole
  fGaindBd           :extended;        //gain of yagi in dBd
  fBeamWidth         :extended;        //beamwidth of yagi to 3dB points
  fDipoleDistance    :extended;        //distance of dipole from reflector

implementation

{$R *.lfm}

Procedure DisplayYagi;               //put details into the memo for display;
Var i:shortint;
begin
    with form1 do
     begin
     Memo1.text:='VK5DJ''s YAGI CALCULATOR'+#13+#10+#13+#10;
     Memo1.Text:=memo1.Text +'GENERAL'+#13+#10;
     Memo1.text:=Memo1.text+'Yagi design frequency ='+FloatToStrF(fFreq,ffFixed,6,2)+' MHz'+#13+#10;
     Memo1.text:=Memo1.text+'Wavelength ='+FloatToStrF(fLambda,ffFixed,7,0)+' mm'+#13+#10;
     if radiogroup1.ItemIndex = 0 then Memo1.Text:=Memo1.Text+'Square boom of width '+edit5.Text+' mm'+#13+#10;
     if radiogroup1.ItemIndex = 1 then Memo1.Text:=Memo1.Text+'Round boom of diameter '+edit5.Text+' mm'+#13+#10;

     if radiogroup4.itemindex = 0 then Memo1.text:=Memo1.text+'Parasitic elements through and contacting a metallic boom'+#13+#10;
     if radiogroup4.itemindex = 1 then Memo1.text:=Memo1.text+'Parasitic elements insulated but through a metallic boom'+#13+#10;
     if radiogroup4.itemindex = 2 then Memo1.text:=Memo1.text+'Parasitic elements on standoffs or a non-metallic boom'+#13+#10;

     if radiogroup5.itemindex=0 then Memo1.text:=Memo1.Text+'Folded dipole through and contacting metal boom'+#13+#10;
     if radiogroup5.itemindex=1 then Memo1.text:=Memo1.text+'Folded dipole insulated but through a metallic boom'+#13+#10;
     if radiogroup5.itemindex=2 then Memo1.text:=Memo1.text+'Folded dipole on standoffs or a non metallic boom'+#13+#10;

     if radiogroup3.ItemIndex=0 then Memo1.Text:=Memo1.Text+'Dipole is round '+edit7.text+' mm diameter'+#13+#10;
     if radiogroup3.ItemIndex=1 then Memo1.Text:=Memo1.Text+'Dipole is square '+edit7.text+' mm wide '+#13+#10;
     if radiogroup3.ItemIndex=2 then Memo1.Text:=Memo1.Text+'Dipole is ribbon '+edit11.text+' mm wide '+edit12.text+' thick'+#13+#10;

     if radiogroup2.ItemIndex = 0 then Memo1.Text:=Memo1.Text+'Parasitic elements are round '+edit6.Text+' mm'+#13+#10;
     if radiogroup2.ItemIndex = 1 then Memo1.Text:=Memo1.Text+'Parasitic elements are square '+edit6.Text+' mm'+#13+#10;
     if radiogroup2.ItemIndex = 2 then Memo1.Text:=Memo1.Text+'Parasitic elements are ribbon '+edit9.Text+' mm wide and '+edit10.text+' mm thick'+#13+#10;

     memo1.Text:=Memo1.Text+#13+#10;

     memo1.Text:=memo1.text + 'REFLECTOR'+#13+#10;
     memo1.Text:=memo1.text + 'Length '+FloatToStrF(fReflectorLength,ffFixed,5,1)+' mm at 30 mm boom position'+#13+#10+#13+#10;

     fBoomLength:= 30.0+fDipoleDistance;
     memo1.Text:=memo1.text + 'DIPOLE'+#13+#10;
     memo1.Text:=memo1.text + 'Single  dipole '+FloatToStrF(fSingleDipoleLength,ffFixed,5,1)+' mm at '+
                 FloatToStrF(fDipoleDistance,ffFixed,5,1)+' mm or boom position '+ FloatToStrF(fBoomLength,ffFixed,5,1)+' mm'+#13+#10;
     memo1.Text:=memo1.text + 'Folded dipole '+FloatToStrF(fFoldedDipoleLength,ffFixed,5,1)+' mm at '+
                 FloatToStrF(fDipoleDistance,ffFixed,5,1)+' mm or boom position '+ FloatToStrF(fBoomLength,ffFixed,5,1)+' mm'+#13+#10+#13+#10;

     for i := 1 to bNoDirectors do                               //place the directors
         begin
             fBoomLength:=fBoomLength + fDirectorDistances[i];  //make boom position
             memo1.Text:=memo1.Text+inttoStr(i)+'   '+FloatToStrF(fDirectorLengths[i],ffFixed,5,1)+' mm at spacing '+
             FloatToStrF(fDirectorDistances[i],ffFixed,5,1)+' mm or boom position '+
             FloatToStrF(fBoomLength,ffFixed,5,1)+' mm'+#13+#10;
         end;

     memo1.Text:=memo1.Text + #13+#10 + 'Note the reflector and the last director are 30mm in from each end of boom'+#13+#10;
     memo1.Text:=memo1.Text + 'so boom has a total length of '+FloatToStrF(fBoomLength+30.0,ffFixed,5,1)+' mm'+#13+#10;
     memo1.Text:=memo1.Text + 'Spacings measured centre to centre of elements'+#13+#10+#13+#10;

     memo1.Text:=memo1.Text + 'GAIN' +#13+#10;
     memo1.Text:=memo1.Text + FloatToStrF(fGaindBd,ffFixed,5,1)+' dBd'+#13+#10+#13+#10;

     memo1.Text:=memo1.Text + 'CUTTING TOLERANCE'+#13+#10;
     memo1.Text:=Memo1.Text + 'Cutting error for elements <'+FloatToStrF(fCuttingAccuracy,ffFixed,5,1)+' mm'+#13+#10+#13+#10;

     memo1.Text:=memo1.Text + 'BEAM WIDTH AT 3dB'+#13+#10;
     memo1.Text:=Memo1.Text + FloatToStrF(fBeamWidth,ffFixed,5,0)+' degrees'+#13+#10;
     end;
End;

Function CalcEquivRibbon(Width,Thick:extended):extended;    //calculate the round equivalent of a flat ribbon element
begin
     result:= width*(0.5 + 0.9*thick/width - 0.22 * power(thick/width,2))/flambda;
end;

function CalculateGain(BoomL:extended):extended;       //gain is related to the length of the boom
Begin
     CalculateGain:= 3.39 * (Ln(BoomL/fLambda)) + 9.15;
End;

function CalculateBeamWidth(GainT:extended):extended; //get the beam width from gain of yagi
Var
     Interim1:extended;
Begin
     GainT:=(GainT + 2.14)/10.0;
     Interim1:= Exp(GainT * 2.303);
     CalculateBeamWidth:= Sqrt(41253.0/Interim1);
End;

procedure BoomCorrection(indexNo:shortint);      //this routine determines the correction distance (in Wavelengths)
begin                                            // due to boom effect on elements
    fAddedLength:= 12.0* fBoomWidth * fBoomWidth + 0.15 * fBoomWidth;
    if fAddedLength > (0.666 * fBoomWidth) then fAddedLength := 0.666 * fBoomWidth;
    if indexno = 1 then faddedLength := fAddedlength / 2.0;
    if indexno = 2 then fAddedLength :=0.0;
end;

function fCalcDir(dirNo:shortint):extended;      //calculate the length of a dipole in wavelengths
Var
   Step1,Step2,Step3:extended;
Begin
     Step1:= 0.5179 - 0.4328 * power(fCrossSectionEl,0.2078);
     Step2:=0.1794 * power(fCrossSectionEl,0.1996);
     Step3:=Exp(DirNo * -0.07586);
     fCalcDir:=Step1 + (0.007344 + Step2) * Step3;
End;

procedure fCalculateDirectors;
var I :integer;
    fCheckthickness:extended;
    fElementSpacing:extended;
begin
    for I := 1 to 100 do                           //reset arrays
        begin
        fDirectorLengths[i]  :=0.0;                //used to store director lengths
        fDirectorDistances[i]:=0.0;                //used to store spacings between directors
        end;
    fBoomLength := 30.0 + fDipoleDistance;         //adjust boomlength ready for director position
    fCheckThickness:=fCalcDir(1)+fAddedLength;     //get thickness of the element
    if fCheckThickness < (fCrossSectionEl * 30.0 / fLambda) then  //check if it's too fat
       begin
           Showmessage('Elements are thicker than ideal, reduce diameter for best results');
       end;
                                                           //main director calculation loop
    for i := 1 to bNoDirectors do                          //count through the wanted directors
        begin                                              //getting length and spacing into storage arrays
            fDirectorLengths[i]:= (fCalcDir(i) + fAddedLength)* fLambda;
            If i < 15 then
               begin
                   fElementSpacing:=Spaces[i]*fLambda      //use table for first 14 directors
               end
               else
               begin
                   fElementSpacing:= 0.4 * fLambda;        //from 15 on use 0.4 Lambda
               end;
            fDirectorDistances[i]:=fElementSpacing;
            fBoomLength:=fBoomLength+fElementSpacing;      //update boomlength Calculation
        end;
     fBoomLength:=fBoomLength+30.0;                        //complete the yagi with a 30mm overhang each end
end;

Procedure CalculateDipole;                                 //calculate the length of the dipole
var fInterim1, fInterim2:extended;                         //temp variables
begin
   fInterim1 := 0.4777 - (1.0522 * fCrossSectionDip);
   fInterim2 := 0.43363 * (power(fCrossSectionDip,-0.014891));
   fSingleDipoleLength:= fLambda*((fInterim1 + fInterim2)/2.0) +fAddedLength;
   fFoldedDipoleLength:=1.02*fSingleDipoleLength;
end;

function CalculateReflector:extended;                      //determine length of reflector in mm
var fInterim1, fInterim2 :extended;
begin
   fDistanceReflector := 0.2* fLambda;
   fInterim1:= Ln(2/fCrossSectionEl)*186.8769 - 320.0;
   fInterim2:= (-20/fInterim1 + 1.0)/2;                    //result in wavelengths
   result:= (fInterim2 + fAddedLength) * fLambda;          //convert to mm
 end;

procedure Calculate;                                       //determines the data for printout
begin
     BoomCorrection(bDirMount);                            //returns fAddedlength
     fReflectorLength := CalculateReflector;
     CalculateDipole;                                      //determines fSingleDipoleLength and fFoldedDipoleLength
     fCalculateDirectors;                                  //provides a 50 extended array DirectorArray[0..49]
     fGaindBd  :=CalculateGain(fBoomLength-60.0);          //get gain of yagi
     fBeamWidth:=CalculateBeamWidth(fGaindBd);             //get beamwidth
     form1.memo1.visible:=true; form1.button2.visible:=true;
     form1.button1.visible:=false;                         //display memo with information
     DisplayYAGI;                                          //put the yagi dimensions in a memo for reading
end;

(* Clicking Button1 ("Calculate") initiates the process. In effect the program starts here*)

procedure TForm1.Button1Click(Sender: TObject);    //bring in the inputs from the entry boxes
begin
     fFreq            := strtofloat(edit1.Text);   //frequency in MHz
     fLambda          := 299792.0 / fFreq;         //wavelength in mm
     fCrossSectionEl  := strtofloat(edit6.Text);   //diameter of element material mm
     fCrossSectionDip := strtofloat(edit7.Text);   //diameter or width of dipole material mm
     fDiamBend        := strtofloat(edit2.Text);   //diameter of folded dipole bend mm
     fDipoleGap       := strtofloat(edit3.Text);   //gap for dipole feed mm
     bNoDirectors     := strtoint(edit4.Text);     //number of directors
     fBoomWidth       := strtofloat(edit5.Text);   //thickness of boom mm
     bBoomtype        := radioGroup1.ItemIndex;    //boom type (round or square)
     bDirShape        := radiogroup2.Itemindex;    //shape of parasitic elements
     bDirMount        := radiogroup4.ItemIndex;    //how parasitics are mounted
     fVelFactor       := strtofloat(edit8.Text);   //velocity factor of coax
     bDipoleShape     := radiogroup3.ItemIndex;    //round,square or flat
     bDipoleMount     := radiogroup5.ItemIndex;    //through, insulated or nonmetallic
     fWidthFlatEl     := strtofloat(edit9.Text);   //parasitic flat measurement mm
     fThickFlatEl     := strtofloat(edit10.Text);  //parasitic thickness of flat mm
     fWidthFlatDip    := strtofloat(edit11.Text);  //dipole flat measurement mm
     fThickFlatDip    := strtofloat(edit12.Text);  //dipole thickness measurment mm

     fCrossSectionEl  :=fCrossSectionEl / fLambda; //width of parasitic elements to wavelengths
     fCrossSectionDip :=fCrossSectionDip /fLambda; //width dipole to wavelengths
     fBoomWidth       :=fBoomWidth / fLambda;      //width of boom in wavelengths

     //now calculate equivalent diameters for square and flat ribbon materials
     if bDirShape = 1 then fCrossSectionEl:= fCrossSectionEl * 1.18;       //equivalent diameter for square element material
     if bDirShape = 2 then                                                 //equivalents for ribbon (director/relector)
        begin
         fCrossSectionEl:= CalcEquivRibbon(fWidthFlatEl,fThickFlatEl);     //calculate the equivalent diameter of ribbon
        end;

     if bBoomType = 0 then fBoomWidth := fBoomWidth * 1.18;                //calculate equivalent boom effect for square

     if bDipoleShape = 1 then fCrossSectionDip := fCrossSectionDip * 1.18; //dipole equivalent diam if square
     if bDipoleShape = 2 then                                              //now equivalent if ribbon
        begin
        fCrossSectionDip:= CalcEquivRibbon(fWidthFlatDip,fThickFlatDip);   //calculate the equivalent diameter of ribbon
        end;

     fCuttingAccuracy := 0.003 * fLambda;          //use if required to provide guide for cutting
     fDipoleDistance  := 0.2 * fLambda;            //distance of dipole from reflector
     Calculate;                                    //now establish all the variables
end;

procedure TForm1.Button2Click(Sender: TObject);    //back button click to hide memo
begin
     memo1.visible:=false; button2.visible:=false; button1.visible:=true;
end;

procedure TForm1.Button3Click(Sender: TObject);
Var VelFactor:extended;
begin
     label14.Visible:=true;
     VelFactor:=strtofloat(Edit8.Text);
     fLambda:= 299792.0/strtofloat(edit1.Text);        //calculate wavelength
     label14.Caption:= FloatToStrF(fLambda / 2.0 * VelFactor,ffFixed,5,0)+' mm'; //half wave * VF
end;

procedure TForm1.RadioGroup2Click(Sender: TObject);    //metal shape of parasitics
begin
  if (radiogroup2.itemindex = 0) or (radiogroup2.itemindex = 1) then
     begin
     label10.Visible:=true;                            //if round or square parasitic material
     edit6.Visible  :=true;
     label16.Visible:=false;
     label17.Visible:=false;
     edit9.Visible  :=false;
     edit10.Visible :=false;
     end;

  if radiogroup2.itemindex = 2 then                    //ribbon material for dipole
     begin                                             //activate two inputs for width and thickness
     label10.Visible:=false;
     edit6.Visible  :=false;
     label16.Visible:=true;
     label17.Visible:=true;
     edit9.Visible  :=true;
     edit10.Visible :=true;
     end;
end;

procedure TForm1.RadioGroup3Click(Sender: TObject);    //metal shape dipole radiogroup
begin
    if (radiogroup3.itemindex = 0) or (radiogroup3.itemindex = 1) then
     begin
     label11.Visible:=true;                            //if round or square dipole material
     edit7.Visible  :=true;
     label18.Visible:=false;
     label19.Visible:=false;
     edit11.Visible  :=false;
     edit12.Visible :=false;
     end;

  if radiogroup3.itemindex = 2 then                    //if ribbon for dipole
     begin                                             //activate two inputs for width and thickness
     label11.Visible :=false;
     edit7.Visible   :=false;
     label18.Visible :=true;
     label19.Visible :=true;
     edit11.Visible  :=true;
     edit12.Visible  :=true;
     end;
end;

end.

